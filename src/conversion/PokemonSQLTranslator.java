/*
 * Author: Denton Wood
 * Project Name: Pokemon Availability
 * Date Created: 12/18/2018
 */

package conversion;

import data.PokemonAvailabilityTableHandler;
import data.tablerecords.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * Translates a CSV file with a given format to an SQL file
 *
 * @version 1.0
 * @author Denton Wood
 */
public class PokemonSQLTranslator {
    public static void main(String[] args) {
        if (args.length != 4) {
            throw new IllegalArgumentException("Expected: <db URL> <db " +
                    "username> <db password> <data filename>");
        }
        String dbUrl = args[0];
        String dbUsername = args[1];
        String dbPassword = args[2];
        String filename = args[3];

        try {
            PokemonAvailabilityTableHandler tableHandler =
                    new PokemonAvailabilityTableHandler(dbUrl, dbUsername,
                            dbPassword);
            List<PokemonGameRecord> games = tableHandler.loadGames();
            List<PokemonTimeRecord> times = tableHandler.loadTimes();
            PokemonCSVLineParser parser = new PokemonCSVLineParser(games,
                    times);

            Scanner csvIn = new Scanner(new File(filename));
            csvIn.useDelimiter("\n");
            // Skip header line
            csvIn.nextLine();
            while (csvIn.hasNextLine()) {
                String nextLine = csvIn.nextLine();
                List<PokemonAvailabilityRecord> availability =
                        parser.parseLine(nextLine);
                tableHandler.insertAvailability(availability);
            }
            tableHandler.insertFamilies(parser.getFamilies());
        } catch (SQLException e) {
            System.err.println("Database error: " + e);
        } catch (IOException e) {
            System.err.println("Cannot open file: " + e);
        }
    }
}
