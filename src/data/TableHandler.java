/*
 * Author: Denton Wood
 * Project Name: Pokemon Availability
 * Date Created: 12/18/2018
 */

package data;

import java.sql.*;

/**
 * Generic handler class to interface with a database
 *
 * @version 1.0
 * @author Denton Wood
 */
public class TableHandler {
    /** The connection to the database */
    private Connection dbConnection;

    /**
     * Constructor for the class. Creates a database connection using the
     * given credentials
     *
     * @param dbUrl the URL of the database
     * @param dbUsername admin username
     * @param dbPassword admin password
     * @throws SQLException if there is an issue with the connection
     */
    public TableHandler(String dbUrl, String dbUsername,
                        String dbPassword) throws SQLException {
        this.dbConnection = DriverManager.getConnection(dbUrl, dbUsername,
                dbPassword);
    }

    /**
     * Loads a table from the database
     *
     * @param tableName the table to load
     * @return a ResultSet containing the loaded table
     * @throws SQLException if loading fails
     */
    public ResultSet loadTable(String tableName) throws SQLException {
        PreparedStatement query = this.dbConnection.prepareStatement("SELECT " +
                "* FROM " + tableName);
        return query.executeQuery();
    }

    public void insertIntoTable(String tableName, String[] columns,
                                String[] data) throws SQLException {
        String syntax = "INSERT INTO " + tableName + "(";
        for (int i = 0; i < columns.length; i++){
            syntax += columns[i] + ", ";
        }
        syntax += ") VALUES ";
        for (int i = 0; i < data.length; i++) {
            syntax += "(\'" + data[i] + "\'),";
        }
        syntax += ";";
        PreparedStatement statement =
                this.dbConnection.prepareStatement(syntax);
        statement.execute();
    }

    public void closeConnection() throws SQLException {
        this.dbConnection.close();
    }
}
